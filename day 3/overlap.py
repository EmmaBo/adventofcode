
def read():
    with open("input.txt", "r") as f:
        for line in f:
            (left,top,width,height) = separate(line)
            #print(left,top,width,height)
            addTile(left,top,width,height)

def separate(line):
    pleft = line.find('@') +2
    ptop = line.find(',')
    pwidth = line.find(':')
    pheight= line.find('x')

    left = int(line[pleft:ptop])
    top = int(line[ptop+1:pwidth])
    width= int(line[pwidth+2:pheight])
    height = int(line[pheight+1:])
    return(left,top,width,height)

def addTile(left, top, width, height):
    for x in range(left,left+width):
        for y in range(top,top+height):
           # print(x,y)
            fabric[x][y] = fabric[x][y] + 1


def count():
    count = 0
    for l in fabric:
        for cell in l:
            if(cell >1):
                count = count + 1
    print(count)



twidth = 2000
fabric = [[0 for col in range(twidth)] for row in range(twidth)]
read()
count()